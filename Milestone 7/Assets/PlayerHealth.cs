﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int maxHP;
    public int currentHP;
 
    void Start()
    {
        currentHP = maxHP;
    }
 
    void ApplyDamage(int value)
    {
        currentHP -= value;
        if (currentHP <= 0)
        {
            Death();
        }
    }

    void Death()
    {
        Debug.Log("Player Killed!");
        Time.timeScale = 0f;
    }

    void OnCollisionEnter(Collision target)
    {
        if(target.gameObject.tag == "Bullet")
        {
            ApplyDamage(20);
        }
    }
}
