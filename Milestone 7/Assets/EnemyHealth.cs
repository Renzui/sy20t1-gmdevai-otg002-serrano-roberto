﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int maxHP = 100;
    public int currentHP = 100;
 
    void Start()
    {
        currentHP = maxHP;
    }
 
    void ApplyDamage(int value)
    {
        currentHP -= value;
        if (currentHP <= 0)
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision target)
    {
        if(target.gameObject.tag == "Bullet")
        {
            ApplyDamage(20);
        }
    }
}
