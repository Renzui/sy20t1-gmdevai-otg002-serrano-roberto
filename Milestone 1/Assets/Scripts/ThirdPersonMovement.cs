﻿using UnityEngine;
using System.Collections;

public class ThirdPersonMovement : MonoBehaviour
{

    public float speed = 3;
    // Update is called once per frame
    void Update()
    {
        
        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        this.transform.position += movement * speed * Time.deltaTime;
        transform.rotation = Quaternion.LookRotation(movement);

        if (movement.magnitude > 1)
        {
            transform.Translate(movement * speed * Time.deltaTime, Space.World);
        }
    }

}